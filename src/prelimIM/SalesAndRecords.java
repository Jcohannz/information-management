package prelimIM;

public class SalesAndRecords {
    private String region;
    private String country;
    private String itemType;
    private String salesChannel;
    private String orderPriority;
    private String orderDate;
    private int orderID;
    private String shipDate;

    public SalesAndRecords(String region, String country, String itemType, String salesChannel, String orderPriority, String orderDate, int orderID, String shipDate) {
        this.region = region;
        this.country = country;
        this.itemType = itemType;
        this.salesChannel = salesChannel;
        this.orderPriority = orderPriority;
        this.orderDate = orderDate;
        this.orderID = orderID;
        this.shipDate = shipDate;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getSalesChannel() {
        return salesChannel;
    }

    public void setSalesChannel(String salesChannel) {
        this.salesChannel = salesChannel;
    }

    public String getOrderPriority() {
        return orderPriority;
    }

    public void setOrderPriority(String orderPriority) {
        this.orderPriority = orderPriority;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public String getShipDate() {
        return shipDate;
    }

    public void setShipDater(String shipDate) {
        this.shipDate = shipDate;
    }

    public String toString() {
        return  "Region: " + region + "\n" +
                "Country: " + country + "\n" +
                "Item Type: " + itemType + "\n" +
                "Sales Channel: " + salesChannel + "\n" +
                "Order Priority: " + orderPriority + "\n" +
                "Order ID: " + orderID + "\n" +
                "Ship Date: " + shipDate;
    }

/*
    public String location() {
        return  region+ ", " + country;
    }

    public int compareTo(SalesAndRecords other) {
        return location().compareToIgnoreCase(other.location());
    }
*/

} // end of class Course

