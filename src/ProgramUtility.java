import prelimIM.SalesAndRecords;

import java.awt.*;
import java.io.*;
import java.util.*;
import java.util.List;

public class ProgramUtility {
    private HashMap<Point, String> _map = new HashMap<Point, String>();
    private int _cols;
    private int _rows;

    public List<SalesAndRecords> readFile(String file) {
        List<SalesAndRecords> sales = new ArrayList<>();
        try (Scanner fReader = new Scanner( new File( file ) )) {
            fReader.nextLine();
            while (fReader.hasNextLine()) {
                String[] data = fReader.nextLine().split( ",(?=([^\"]*\"[^\"]*\")*[^\"]*$)" );
                SalesAndRecords s = new SalesAndRecords( data[0], data[1], data[2], data[3], data[4], data[5], Integer.parseInt( data[6] ), data[7] );
                sales.add( s );
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sales;
    }

    public void sortListLocation(List<SalesAndRecords> data) {
        List<SalesAndRecords> duplicate = new ArrayList<>( data );
        Comparator<SalesAndRecords> comparator = (s1, s2) -> {
            if (s1.getRegion().compareToIgnoreCase( s2.getRegion() ) == 0) {
                return s1.getCountry().compareToIgnoreCase( s2.getCountry() );
            }
            return s1.getRegion().compareToIgnoreCase( s2.getRegion() );
        };
        duplicate.sort( comparator );
        for (int i = 0; i < data.size(); i++) {
            System.out.println( duplicate.get( i ) );
            System.out.println( "---------------------------------------------------" );
        }
    }

    public void sortListOrderID(List<SalesAndRecords> file) {
        List<SalesAndRecords> s = new ArrayList<>( file );
        s.sort( new Comparator<SalesAndRecords>() {
            @Override
            public int compare(SalesAndRecords p1, SalesAndRecords p2) {
                return p1.getOrderID() - p2.getOrderID();
            }
        } );
        for (int i = 0; i < file.size(); i++) {
            System.out.println( s.get( i ) );
            System.out.println( "---------------------------------------------------" );
        }
    }

    public void sortListItemType(List<SalesAndRecords> data) {
        List<SalesAndRecords> duplicate1 = new ArrayList<>( data );
        Comparator<SalesAndRecords> comparator = (s1, s2) -> {
            if (s1.getItemType().compareToIgnoreCase( s2.getItemType() ) == 0) {
                return s1.getItemType().compareToIgnoreCase( s2.getItemType() );
            }
            return s1.getItemType().compareToIgnoreCase( s2.getItemType() );
        };
        duplicate1.sort( comparator );
        for (int i = 0; i < data.size(); i++) {
            System.out.println( duplicate1.get( i ) );
            System.out.println( "---------------------------------------------------" );
        }
    }
    /*public void NumberOfResident() throws IOException {

       MyProgramUtility resident = new MyProgramUtility();
       int y = 0;
       int g = 0;
       int j = 1;
       int res = 0;
       int nonres = 0;
       File file = new File("data.csv");
       Scanner kbd = new Scanner(file);
       resident.open(file);

       BufferedReader read = new BufferedReader(new FileReader(file));
       read.readLine();
       while (kbd.hasNextLine()) {
           String[] value = kbd.nextLine().split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
           String p = "";
           y = y + 1;
           System.out.println();
           resident.put(5, y, p);
           if(value[6].equals("1") && value[5].equals("Resident")){
               res++;
           }else if(value[6].equals("1") && value[5].equals("Non-Resident")){
               nonres++;
           }else if(value[6].equals("2") && value[5].equals("Resident")){
               res
           }else if(value[6].equals("2") && value[5].equals("Resident")){

           }
       }
       System.out.println("District 1\n Resident: " + res);
       System.out.println("Non-Resident: " + nonres);
       System.out.println("----------------------------------");
       System.out.println();
       System.out.println("Press enter to continue!");


   }*/
    /*public void sortListDistrict(List<Citizen> file) {
        Map<Integer, Integer> district  = new TreeMap<>();
        for (Citizen c: file) {
            if (district.containsKey(c.getDistrict())) {
                district.put(c.getDistrict(), district.get(c.getDistrict()) + 1);
            } else {
                district.put(c.getDistrict(), 1);
            }
        }
        System.out.println("Total District: " + district.size());
        System.out.print("Press Enter To View The List of District...");
        new Scanner(System.in).nextLine();

        int totCitizen = 0;
        for (Map.Entry<Integer, Integer> e : district.entrySet()) {
            String d = "District";
            String c = "Citizens";
            System.out.printf("%s %-2s:  %-3d",d, e.getKey(), e.getValue());totCitizen += e.getValue();
            System.out.print(c);
            System.out.println();
        }
        System.out.println("The Total Citizens In All District Are : " + totCitizen + "\n");
        System.out.println("---------------------------------------------------");

    }*/
  /*  public void NumberOfGender() throws IOException {

        ProgramUtility csv = new ProgramUtility();
        int y = 0;
        int g = 0;
        int j = 1;
        int male = 0;
        int female = 0;
        File file = new File("res//SalesRecords.csv");
        Scanner kbd = new Scanner(file);
        csv.open(file);

        BufferedReader read = new BufferedReader(new FileReader(file));
        read.readLine();
        while (kbd.hasNextLine()) {
            String[] value = kbd.nextLine().split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
            String p = "";
            y = y + 1;
            csv.put(5, y, p);
            if(value[7].equals("Male")){
                male++;
            }else{
                female++;
            }

        }
        System.out.println("Male: " + male);
        System.out.println("Female: " + female);
        System.out.println("----------------------------------");
        System.out.println();
    }*/

   /* public void sortListStageOfLife(List<Citizen> file) {
        Scanner kbd = new Scanner(System.in);
        int choice = 0;
        Map<Integer, Integer> age = new TreeMap<>();
        for (Citizen a : file) {
            if (age.containsKey(a.getAge())) {
                age.put(a.getAge(), age.get(a.getAge()) + 1);
            } else {
                age.put(a.getAge(), 1);
            }
        }

        for (Map.Entry<Integer, Integer> e : age.entrySet()) {
            if (e.getKey() <= 24 && e.getKey() > 17) {
                totCitizen += e.getValue();
            }else if (e.getKey() <= 64 && e.getKey() > 24) {
                totCitizen1 += e.getValue();
            }else  if (e.getKey() > 64) {
                totCitizen2 += e.getValue();
            }
        }
        do {
            System.out.println("Choose your desire Age range");
            System.out.println("1. Teenager");
            System.out.println("2. Adults");
            System.out.println("3. Senior Citizen");
            System.out.println("4. Total of Citizen");
            System.out.println("5. Back");
            try {
                choice = kbd.nextInt();
            }catch (InputMismatchException e){
                System.out.println("You have to type an integer!");
                kbd.next();
            }
            switch (choice) {
                case 1:
                    // All Teenager Citizens from 18-24 years old
                    System.out.print("Press Enter To View The Number of Teenagers...");
                    new Scanner(System.in).nextLine();
                    for (Map.Entry<Integer, Integer> e : age.entrySet()) {
                        String d = "Age of";
                        String c = "Citizens";

                        if (e.getKey() <= 24 && e.getKey() > 17) {
                            System.out.printf("%s %-2s:          %-3d", d, e.getKey(), e.getValue());
                            System.out.println();
                        }
                    }
                    System.out.println("------------------------------------------------------");
                    System.out.println("The Total Teenagers Are : " + totCitizen + " Citizens" + "\n");
                    System.out.println("---------------------------------------------------");
                    break;
                case 2:
                    // All Adult Citizens from 25-64 years old
                    System.out.print("Press Enter To View The Number of Adults...");
                    new Scanner(System.in).nextLine();
                    for (Map.Entry<Integer, Integer> e : age.entrySet()) {
                        String d = "Age of";
                        String c = "Citizens";
                        if (e.getKey() <= 64 && e.getKey() > 24) {
                            System.out.printf("%s %-2s:          %-3d", d, e.getKey(), e.getValue());
                            System.out.println();
                        }
                    }
                    System.out.println("------------------------------------------------------");
                    System.out.println("The Total Adults Are : " + totCitizen1 + " Citizens" + "\n");
                    System.out.println("---------------------------------------------------");
                    break;
                case 3:
                    // All Senior Citizens from 65 years old and above
                    System.out.print("Press Enter To View The Number of Seniors...");
                    new Scanner(System.in).nextLine();
                    for (Map.Entry<Integer, Integer> e : age.entrySet()) {
                        String d = "Age of";
                        String c = "Citizens";
                        if (e.getKey() > 64) {
                            System.out.printf("%s %-2s:          %-3d", d, e.getKey(), e.getValue());
                            System.out.println();
                        }
                        System.out.println("------------------------------------------------------");
                        System.out.println("The Total Seniors Are : " + totCitizen2 + " Citizens" + "\n");
                    }
                    break;
                case 4:

                    System.out.println("The Total Citizens Are: " + (totCitizen + totCitizen1 + totCitizen2) + " Citizens");
                    System.out.println("---------------------------------------------------");
                    break;
            }
        }while (choice != 5);
    }*/
    public void open(File file) throws IOException {
        open( file, ',' );
    }
    public void open(File file, char delimiter)
            throws FileNotFoundException, IOException {
        Scanner scanner = new Scanner( file );
        scanner.useDelimiter( Character.toString( delimiter ) );

        _map.clear();

        while (scanner.hasNextLine()) {
            String[] values = scanner.nextLine().split( Character.toString( delimiter ) );

            int col = 0;
            for (String value : values) {
                _map.put( new Point( col, _rows ), value );
                _cols = Math.max( _cols, ++col );
            }
            _rows++;
        }
        scanner.close();
    }
    public void save(String file) throws IOException {
        save( new File( file ), ',' );
    }
    public void save(File file, char delimiter) throws IOException {
        FileWriter fw = new FileWriter( file );
        BufferedWriter bw = new BufferedWriter( fw );

        for (int row = 0; row < _rows; row++) {
            for (int col = 0; col < _cols; col++) {
                Point key = new Point( col, row );
                if (_map.containsKey( key )) {
                    bw.write( _map.get( key ) );
                }

                if ((col + 1) < _cols) {
                    bw.write( delimiter );
                }
            }
            bw.newLine();
        }
        bw.flush();
        bw.close();
    }
    public String get(int col, int row) {
        String val = "";
        Point key = new Point( col, row );
        if (_map.containsKey( key )) {
            val = _map.get( key );
        }
        return val;
    }
    public void put(int col, int row, String value) {
        _map.put( new Point( col, row ), value );
        _cols = Math.max( _cols, col + 1 );
        _rows = Math.max( _rows, row + 1 );
    }
    public void clear() {
        _map.clear();
        _cols = 0;
        _rows = 0;
    }
    public int rows() {
        return _rows;
    }
    public int cols() {
        return _cols;
    }
}

