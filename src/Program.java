import prelimIM.SalesAndRecords;

import java.io.*;
import java.util.*;
import java.util.List;

public class Program {

    public static void main(String[] args) throws IOException {
        ProgramUtility mp = new ProgramUtility();
        Scanner kbd = new Scanner(System.in);
        int choice = 0;

        List<SalesAndRecords> sales = mp.readFile("res//SalesRecords.csv");
        do {
            menu();
            try {
                choice = kbd.nextInt();
            }catch (InputMismatchException e){
                System.out.println("You have to type an integer!");
                kbd.next();
            }


            switch (choice) {
                case 1:
                    mp.sortListLocation(sales);
                    break;
                case 2:
                    mp.sortListOrderID(sales);
                    break;
                case 3:
                    mp.sortListItemType(sales);
                    break;
            }
        }while (choice != 4);
    }
    public static void menu(){
        System.out.println("----------Main Menu----------");
        System.out.println("|   1. Sort by Region       |");
        System.out.println("|   2. Sort by order ID     |");
        System.out.println("|   3. Sort by item type    |");
        System.out.println("|   4. Exit                 |");
        System.out.println("-----------------------------");
        System.out.println();
        System.out.print("Please enter your choice: ");

    }

}

